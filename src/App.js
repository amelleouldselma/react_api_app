import logo from './logo.svg';
import './App.css';
import React, {useState} from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from './Home';
import all from './all';
import garde from './garde';
import add from './add';

// function api() {
//   axios.get('http://127.0.0.1:40459/pharma')
//   .then(function (response) {
//     // handle success
//     console.log(response);
//   })
//   .catch(function (error) {
//     // handle error
//     console.log(error);
//   })
// }

function App() {
  const [show, setShow]=useState(true)

  return (
    <div className="App">
      <header>        
        <div style={{height: '150px',fontStyle: 'italic', fontSize:'30px',position: 'static'}}>
          <p>Welcome on</p>
          <p>Ma Pharmapi Client en React</p>
        </div>
      </header>
      <nav className="App-header">
        <Router>
          <div>
            <nav>
              <ul>
                <li><Link to={'/'} className="nav-link"> Home </Link></li>
                <Link to={'/all'} className="nav-link"><button onClick={()=>setShow(!show)}>Toute les pharmacies</button></Link>
                <Link to={'/garde'} className="nav-link"><button onClick={()=>setShow(!show)}>La pharmacie de garde</button></Link>
                <Link to={'/add'} className="nav-link"><button onClick={()=>setShow(!show)}>Ajouter une pharmacie</button></Link>
              </ul>
            </nav>
            <hr />
            <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/all' component={all} />
                <Route path='/garde' component={garde} />
                <Route path='/add' component={add} />
            </Switch>
          </div>
        </Router>

        <img src={logo} className="App-logo" alt="logo" />

      </nav>
      <footer> 
        <div style={{height: '150px'}}>
            <p>© 2021 Copyright: Amelle </p>
        </div>
      </footer>
      {
        show?<h1 style={{ height: '50px' }}>Hello</h1>:null
      }
      <button onClick={()=>setShow(!show)}>Toggle</button>
    </div>
  );

}

export default App;

