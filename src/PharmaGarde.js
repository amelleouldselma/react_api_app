import React from 'react';
import axios from "axios";

// export function api() {
//   const result = axios
//     .get(
//       "http://127.0.0.1:40459/pharma"
//     )
//     .then(({ data }) => data);
//   return result;
// };


class PharmaGarde extends React.Component {

    constructor(props){
        super(props);
        this.state = {pharmacies: []};
    
        // this.render = this.render.bind(this);
        // this.componentDidMount = this.componentDidMount.bind(this);
      }
    


  componentDidMount() {
    axios.get(`https://afternoon-sea-43781.herokuapp.com/pharma-garde`)
      .then(res => {
         const pharmapi = res.data;
        console.log(pharmapi.data);
        console.log(pharmapi);
        this.setState({ pharmacies: pharmapi});
      })
  }

  render() {
    return (
        <div>
        { this.state.pharmacies.map(pharmacies => 
          <div style={{ border: '1px solid #CCC', borderRadius:'10px' }}>
              <h2>{pharmacies.nom}</h2>
              <p>{pharmacies.quartier}</p>
              <p>{pharmacies.ville}</p>
              <p>{pharmacies.garde}</p>
          </div>
        )}
    </div>
    )
  }
}
export default PharmaGarde;
