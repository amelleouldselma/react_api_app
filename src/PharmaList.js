import React from 'react';
import axios from "axios";
import PharmaEdit from './PharmaEdit';


// export function api() {
//   const result = axios
//     .get(
//       "http://127.0.0.1:40459/pharma"
//     )
//     .then(({ data }) => data);
//   return result;
// };


class PharmaList extends React.Component {

    constructor(props){
        super(props);
        this.state = {pharmacies: [],
                      edit : false,
                      idPharma : 0
        };
        
    
        // this.render = this.render.bind(this);
        // this.componentDidMount = this.componentDidMount.bind(this);
      }

  componentDidMount() {
    axios.get(`https://afternoon-sea-43781.herokuapp.com/pharma`)
      .then(res => {
         const pharmapi = res.data;
        console.log(pharmapi.data);
        console.log(pharmapi);
        this.setState({ pharmacies: pharmapi});
      })
  }

  deleteRow(id, e, nom){

    axios.delete(`https://afternoon-sea-43781.herokuapp.com/pharma/${id}`)

      .then(res => {

        console.log(res);
        console.log(res.data);
        const pharmacies = this.state.pharmacies.filter(pharmacies => pharmacies.id !== id);
        this.setState({ pharmacies });
        alert(`${nom} à bien été supprimer`)
        // alert( pharmacies.nom + 'à bien été supprimer')
      })
  }


  editPharma(id, name){
    this.setState({modify: true});
    axios.put('https://afternoon-sea-43781.herokuapp.com/pharma/${id}')
          .then(()=> console.log(`La pharmacie ${id} à été modifiée`), alert(`${name} modifiée`))
          .catch(function (error) {
            console.log(error);
          });
  }


  render() {
    return (
        <div>
          {this.state.edit === false && 
            <><ul>
              { this.state.pharmacies.map(pharmacies => 
                <div  style={{border: '1px solid #CCC', borderRadius:'10px'}} key={pharmacies.id}>
                    <h2>{pharmacies.nom}</h2>
                    <p>{pharmacies.quartier}</p>
                    <p>{pharmacies.ville}</p>
                    <p>{pharmacies.garde}</p>
                    <td>
                      <button onClick={(e) => this.deleteRow(pharmacies.id, e)}>Supprimer</button>
                    </td>
                    <td>
                      <button title="edit" onClick={() => this.setState({edit : true, idPharma : pharmacies.id })}>Editer</button>
                    </td>
                </div>
              )}
              </ul></>
            }  
            {this.state.edit === true && 
            <PharmaEdit id={this.state.idPharma}/>}
        </div>
    )
  }
}
export default PharmaList;